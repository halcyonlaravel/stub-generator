<?php

namespace {{ namespace }};

use App\Models\Traits\AuditTrait;
use HalcyonLaravelBoilerplate\CoreBase\Models\ActionLinks\ActionLink;
use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\PermissionInterface;
use HalcyonLaravelBoilerplate\CoreBase\Models\Contracts\ResourceConfigInterface;
use HalcyonLaravelBoilerplate\CoreBase\Models\Traits\ActionLinkTrait;
use HalcyonLaravelBoilerplate\CoreBase\Models\Traits\ForHasStatusesEnableDisable;
use HalcyonLaravelBoilerplate\CoreBase\Models\Traits\GenerateResourceConfigTrait;
use HalcyonLaravelBoilerplate\CoreBase\Models\Traits\TimeZoneForUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * {{ namespace }}\{{ class }}
 *
 * @property int $id
 * @property string $title
 * @property string $segment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static Builder|{{ class }} newModelQuery()
 * @method static Builder|{{ class }} newQuery()
 * @method static \Illuminate\Database\Query\Builder|{{ class }} onlyTrashed()
 * @method static Builder|{{ class }} query()
 * @method static bool|null restore()
 * @method static Builder|{{ class }} whereCreatedAt($value)
 * @method static Builder|{{ class }} whereDeletedAt($value)
 * @method static Builder|{{ class }} whereId($value)
 * @method static Builder|{{ class }} whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|{{ class }} withTrashed()
 * @method static \Illuminate\Database\Query\Builder|{{ class }} withoutTrashed()
 * @mixin \Eloquent
 */
class {{ class }} extends Model implements Auditable, PermissionInterface, ResourceConfigInterface
{
    use AuditTrait;
    use ActionLinkTrait;
    use SoftDeletes;
    use TimeZoneForUser;
    use GenerateResourceConfigTrait;
    use ForHasStatusesEnableDisable;
    use HasSlug;

    protected $fillable = [
        'title',
        'segment',
    ];

    /**
     * @return string
     */
    public static function icon(): string
    {
        // TODO: update value in {{ namespace }}\{{ class }}::icon()
        return 'fa-stack-exchange';
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo($this->getRouteKeyName());
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'segment';
    }

    /**
     * @inheritDoc
     */
    public static function accessPermissions(): array
    {
        return [
            // resources
            'index'   => '{{ model name }} list',
            'create'  => '{{ model name }} create',
            'edit'    => '{{ model name }} edit',
            'show'    => '{{ model name }} show',
            'destroy' => '{{ model name }} destroy',

            // deletes
            'deleted' => '{{ model name }} deleted list',
            'restore' => '{{ model name }} restore',
            'purge'   => '{{ model name }} purge',

            // history
//            'history list' => '{{ model name }} history list',
        ];
    }

    public function actionLinks(): ActionLink
    {
        return ActionLink::create()
            ->model($this)
            ->addResource()
            ->addSoftDelete();
    }
}

<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use InvalidArgumentException;

class Helper
{
    public const PREFIX_COMMAND = 'halcyon:';
    public const PROJECT_NAME = 'Halcyon Laravel Boilerplate';

    public static function buildModelReplacements(
        string $modelClass,
        array $replace = [],
        bool $ignoreCheckModel = false
    ): array {
        if (!$ignoreCheckModel && !class_exists($modelClass)) {
            Artisan::call(self::PREFIX_COMMAND.'make:model', ['name' => $modelClass]);
        }

        // model name
        $modelName = str_replace('_', ' ', Str::lower(Str::snake(class_basename($modelClass))));

        return array_merge(
            $replace,
            [
//                '{{ namespace }}' => '',
//                '{{ class }}' => '',

                '{{ rootNamespace }}' => app()->getNamespace(),
                'NamespacedDummyModel' => $modelClass,
                '{{ namespacedModel }}' => $modelClass,
                '{{namespacedModel}}' => $modelClass,
                'DummyModelClass' => class_basename($modelClass),
                '{{ model }}' => class_basename($modelClass),
                '{{model}}' => class_basename($modelClass),
                'DummyModel' => class_basename($modelClass),
                'DummyModelVariable' => lcfirst(class_basename($modelClass)),
                '{{ modelVariable }}' => lcfirst(class_basename($modelClass)),
                '{{modelVariable}}' => lcfirst(class_basename($modelClass)),

                // additional
                '{{ model name }}' => $modelName,
                '{{ model-name }}' => Str::kebab($modelName),
                '{{ model-name-plural }}' => Str::plural(Str::kebab($modelName)),
                '{{ modelPlural }}' => Str::plural(class_basename($modelClass)),
            ]
        );
    }

    public static function parseModel(string $model): string
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        $model = trim(str_replace('/', '\\', $model), '\\');

        return 'Models\\'.$model.'\\'.$model;
    }
}

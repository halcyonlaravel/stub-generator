<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;

//use Symfony\Component\Console\Input\InputOption;

/**
 * @todo https://github.com/spatie/laravel-signal-aware-command/blob/master/composer.json#L19 (requires php8)
 */
class ResourceGenerateCommand extends Command
{
    protected $name = Helper::PREFIX_COMMAND.'make:crudp';

    protected $description = 'Create a new complete scaffolding for model resource with soft delete.';

//    protected function getOptions()
//    {
//        return [
//            ['with-purge', 'p', InputOption::VALUE_OPTIONAL, 'Generate a resource controller class.'],
//        ];
//    }

    public function handle()
    {
        $name = $this->argument('name');

        $parsedModel = Helper::parseModel($name);

        $this->call(Helper::PREFIX_COMMAND.'make:model', ['name' => $parsedModel]);
        $this->call(
            Helper::PREFIX_COMMAND.'make:repository-observer',
            ['model' => $name]
        );
        $this->call(
            Helper::PREFIX_COMMAND.'make:policy',
            ['name' => $name.'Policy', '--model' => str_replace('Models\\', '', $parsedModel)]
        );

        $this->call(Helper::PREFIX_COMMAND.'make:route:backend', ['model' => $name]);
        $this->call(Helper::PREFIX_COMMAND.'make:breadcrumbs', ['model' => $name]);

        $this->generateRepositories($name, $parsedModel);

        $this->call('make:binding', ['name' => "$name\\$name"]);

        $this->generateControllers($name, $parsedModel);

        $this->call(Helper::PREFIX_COMMAND.'make:livewire-datatable', ['model' => $name]);

        $this->call(Helper::PREFIX_COMMAND.'make:view-blade:backend', ['model' => $name]);

        $this->generateTests($name, $parsedModel);

        // last, it has composer dump-autoload
        $this->generateDatabase($name, $parsedModel);

        $this->output->success('CRUDP Generated successfully!');

        $this->output->note("Kindly place [{$name}TableSeeder] to Database\Seeders\DatabaseSeeder");
        $this->output->note("Kindly add model [$name] to Database\Seeders\Auth\PermissionTableSeeder");
        $this->output->note("Kindly add model observer [{$name}Observer] to App\Listeners\RepositoryActionListener");
        $this->output->note('run composer clear-all');

        parent::call('inspire');
    }

    public function call($command, array $arguments = [])
    {
        $this->output->newLine();
        $args = '';

        foreach ($arguments as $key => $value) {
            if (strpos($key, '--') === false) {
                $args .= " $value";
                continue;
            }

            $v = is_bool($value)
                ? ''
                : '='.$value;

            $args .= " $key$v";
        }

        $this->output->text('php artisan '.$command.$args);

        return parent::call($command, $arguments);
    }

    protected function getArguments(): array
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the model class'],
        ];
    }

    protected function generateDatabase(string $name, string $modelName)
    {
        $this->call(
            Helper::PREFIX_COMMAND.'make:factory',
            [
                'name' => $modelName.'Factory',
                '--model' => app()->getNamespace().$modelName,
            ]
        );

        $this->call(
            Helper::PREFIX_COMMAND.'make:seeder',
            [
                'name' => $name.'TableSeeder',
                '--model' => $modelName,
            ]
        );

        $this->call(
            Helper::PREFIX_COMMAND.'make:migration',
            [
                'name' => 'create_'.Str::snake(Str::pluralStudly($name)).'_table',
            ]
        );
    }

    protected function generateTests(string $name, string $parsedModel)
    {
        $this->call(
            Helper::PREFIX_COMMAND.'make:test',
            [
                'name' => "CRUDP\\$name\\ResourceTest",
                '--type' => 'resource',
                '--model' => $parsedModel,
            ]
        );

        $this->call(
            Helper::PREFIX_COMMAND.'make:test',
            [
                'name' => "CRUDP\\$name\\DeletedTest",
                '--type' => 'deleted',
                '--model' => $parsedModel,
            ]
        );

        $this->call(
            Helper::PREFIX_COMMAND.'make:test',
            [
                'name' => "CRUDP\\$name\\StatusTest",
                '--type' => 'status',
                '--model' => $parsedModel,
            ]
        );
    }

    protected function generateRepositories(string $name, string $modelName)
    {
        $className = 'App\\Repositories\\'.$name.'\\'.$name.'Repository';

        $this->call(
            Helper::PREFIX_COMMAND.'make:repository',
            [
                'name' => $className.'Eloquent',
                '--model' => $modelName,
            ]
        );

        $this->call(
            Helper::PREFIX_COMMAND.'make:repository',
            [
                'name' => $className,
                '--interface' => true,
            ]
        );
    }

    protected function generateControllers(string $name, string $modelName)
    {
        $this->call(
            Helper::PREFIX_COMMAND.'make:controller:backend',
            [
                'name' => 'Backend\\'.$name.'\\'.Str::plural($name).'Controller',
                '--model' => $modelName,
                '--no-interaction' => true,
                '--resource' => true,
            ]
        );

//        $this->call(
//            Helper::PREFIX_COMMAND.'make:controller:backend',
//            [
//                'name' => 'Backend\\'.$name.'\\'.$name.'TableController',
//                '--model' => $modelName,
//                '--no-interaction' => true,
//                '--table' => true,
//            ]
//        );

        $this->call(
            Helper::PREFIX_COMMAND.'make:controller:backend',
            [
                'name' => 'Backend\\'.$name.'\\'.$name.'DeletedController',
                '--model' => $modelName,
                '--no-interaction' => true,
                '--delete' => true,
            ]
        );
    }
}

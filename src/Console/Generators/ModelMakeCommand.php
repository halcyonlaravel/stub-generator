<?php


namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;
use Illuminate\Console\GeneratorCommand;

class ModelMakeCommand extends GeneratorCommand
{
    protected $name = Helper::PREFIX_COMMAND.'make:model';

    protected $description = 'Create a new model with complete useful functionality for '.Helper::PROJECT_NAME;

    protected $type = 'Model';

    protected function getStub(): string
    {
        return __DIR__.'/../../stubs/model.stub';
    }

    protected function buildClass($name)
    {
        $replace = Helper::buildModelReplacements($name, [], true);

        return str_replace(
            array_keys($replace),
            array_values($replace),
            parent::buildClass($name)
        );
    }
}

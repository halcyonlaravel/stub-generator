<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;

class RepositoryObserverMakeCommand extends BaseBasicGenerator
{
    protected $name = Helper::PREFIX_COMMAND.'make:repository-observer';

    protected $description = 'Create a new repository observer for '.Helper::PROJECT_NAME;

    protected string $type = 'Repository Observer';

    public function stubs(string $name): array
    {
        return
            [
                __DIR__.'/../../stubs/observer.stub'
                => $this->laravel->basePath(
                    'app/RepositoryObservers/'.$this->getModelInput().'/'.$this->getModelInput().'Observer.php'
                ),
            ];
    }
}

<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;

class TestMakeCommand extends \Illuminate\Foundation\Console\TestMakeCommand
{
    protected $signature = Helper::PREFIX_COMMAND.'make:test {name : The name of the class} '.
    '{--type=resource : Create a feature resource/deleted/status test} '.
    '{--model= : Add model to test}';

    protected $description = 'Create a new test class for '.Helper::PROJECT_NAME;

    protected function getStub(): string
    {
        switch ($this->option('type')) {
            case 'status':
                $stub = __DIR__.'/../../stubs/test/feature/status.stub';
                break;
            case 'resource':
                $stub = __DIR__.'/../../stubs/test/feature/resource.stub';
                break;
            case 'deleted':
                $stub = __DIR__.'/../../stubs/test/feature/deleted.stub';
                break;
            default:
                $this->error('--type must `resource` or `deleted`');
                die();
        }

        return $stub;
    }

    protected function getDefaultNamespace($rootNamespace): string
    {
        return $rootNamespace.'\Feature';
    }

    protected function replaceClass($stub, $name)
    {
        if ($model = $this->option('model')) {
            $replace = Helper::buildModelReplacements(
                app()->getNamespace().$model
            );

            return str_replace(
                array_keys($replace),
                array_values($replace),
                parent::replaceClass($stub, $name)
            );
        }

        return parent::replaceClass($stub, $name);
    }
}

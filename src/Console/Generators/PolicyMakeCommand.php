<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;

class PolicyMakeCommand extends \Illuminate\Foundation\Console\PolicyMakeCommand
{
    protected $name = Helper::PREFIX_COMMAND.'make:policy';

    protected $description = 'Create a new policy class for '.Helper::PROJECT_NAME;

    protected function getStub(): string
    {
        return $this->option('model')
            ? __DIR__.'/../../stubs/policy.stub'
            : $this->resolveStubPath('/stubs/policy.plain.stub');
    }
}

<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

abstract class BaseBasicGenerator extends Command
{
    protected Filesystem $files;

    protected string $type = '';

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    abstract public function stubs(string $name): array;

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $modelName = $this->getModelInput();

        foreach ($this->stubs($modelName) as $from => $to) {
            $stub = $this->files->get($from);

            $this->makeDirectory($to);
            $this->files->put($to, $this->replaceStubs($stub, $modelName));
        }

        $this->info($this->type.' created successfully.');
    }

    protected function replaceStubs($stub, string $modelName)
    {
        $replace = Helper::buildModelReplacements($this->laravel->getNamespace().Helper::parseModel($modelName));
        return str_replace(
            array_keys($replace),
            array_values($replace),
            $stub
        );
    }

    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }

    protected function getModelInput(): string
    {
        return trim($this->argument('model'));
    }

    protected function getArguments(): array
    {
        return [
            ['model', InputArgument::REQUIRED, 'The name of the model class'],
        ];
    }
}

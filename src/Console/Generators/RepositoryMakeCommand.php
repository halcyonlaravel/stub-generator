<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class RepositoryMakeCommand extends GeneratorCommand
{
    protected $name = Helper::PREFIX_COMMAND.'make:repository';

    protected $description = 'Create a new repository for '.Helper::PROJECT_NAME;

    protected function getStub(): string
    {
        if ($this->option('interface')) {
            $this->type = 'Repository';
            return __DIR__.'/../../stubs/repository.stub';
        }

        $this->type = 'Repository eloquent';

        return __DIR__.'/../../stubs/repository.eloquent.stub';
    }

    protected function buildClass($name)
    {
        if (!$this->option('model')) {
            return parent::buildClass($name);
        }

        $replace = Helper::buildModelReplacements($this->option('model'));

        return str_replace(
            array_keys($replace),
            array_values($replace),
            parent::buildClass($name)
        );
    }


    protected function getOptions(): array
    {
        return
            [
                ['model', 'm', InputOption::VALUE_REQUIRED, 'Generate a resource controller for the given model.'],
                ['interface', null, InputOption::VALUE_NONE, 'Generate interface class instead.'],
            ];
    }
}

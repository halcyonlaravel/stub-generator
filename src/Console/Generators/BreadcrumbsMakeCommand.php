<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;
use Illuminate\Support\Str;

class BreadcrumbsMakeCommand extends BaseBasicGenerator
{
    protected $name = Helper::PREFIX_COMMAND.'make:breadcrumbs';

    protected $description = 'Create a new breadcrumbs for '.Helper::PROJECT_NAME;

    protected string $type = 'Breadcrumbs';

    public function stubs(string $name): array
    {
        return
            [
                __DIR__.'/../../stubs/backend/breadcrumb/resource-delete.php.stub'
                => $this->laravel->basePath('routes/breadcrumbs/backend/'.Str::kebab($this->getModelInput()).'.php'),
            ];
    }
}

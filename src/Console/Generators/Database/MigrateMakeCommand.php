<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Database;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;

class MigrateMakeCommand extends \Illuminate\Database\Console\Migrations\MigrateMakeCommand
{
    protected $signature = Helper::PREFIX_COMMAND.'make:migration {name : The name of the migration}
        {--create= : The table to be created}
        {--table= : The table to migrate}
        {--path= : The location where the migration file should be created}
        {--realpath : Indicate any provided migration file paths are pre-resolved absolute paths}
        {--fullpath : Output the full path of the migration}';

    protected $description = 'Create a new migration file for '.Helper::PROJECT_NAME;
}

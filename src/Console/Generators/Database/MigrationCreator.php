<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Database;

class MigrationCreator extends \Illuminate\Database\Migrations\MigrationCreator
{
    /**
     * @param  string|null  $table
     * @param  bool  $create
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub($table, $create): string
    {
        $stub = $this->customStubPath.'/migration.create.stub';

        return $this->files->get($stub);
    }
}

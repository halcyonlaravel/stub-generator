<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Database;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class FactoryMakeCommand extends \Illuminate\Database\Console\Factories\FactoryMakeCommand
{
    protected $name = Helper::PREFIX_COMMAND.'make:factory';

    protected $description = 'Create a new model factory for '.Helper::PROJECT_NAME;

    protected function getStub(): string
    {
        return __DIR__.'/../../../stubs/database/factory.stub';
    }

    protected function qualifyClass($name): string
    {
        return parent::qualifyClass($name);
    }

    protected function getPath($name): string
    {
        $name = Str::replaceFirst(app()->getNamespace().'Models\\', '', $name);

        return $this->laravel->databasePath().'/factories/'.str_replace('\\', '/', $name).'.php';
    }

    protected function getOptions(): array
    {
        return
            [
                ['model', 'm', InputOption::VALUE_REQUIRED, 'The name of the model'],
            ];
    }
}

<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Database;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class SeederMakeCommand extends \Illuminate\Database\Console\Seeds\SeederMakeCommand
{
    protected $name = Helper::PREFIX_COMMAND.'make:seeder';

    protected $description = 'Create a new seeder class for '.Helper::PROJECT_NAME;

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        // to skip autoload on parent class
        GeneratorCommand::handle();
    }

    protected function getStub(): string
    {
        return __DIR__.'/../../../stubs/database/seeder.stub';
    }

    protected function replaceNamespace(&$stub, $name): self
    {
        parent::replaceNamespace($stub, $name);

        $name = $this->rootNamespace().$this->option('model');

        $replace = Helper::buildModelReplacements($name, [], true);

        $stub = str_replace(
            array_keys($replace),
            array_values($replace),
            $stub
        );

        return $this;
    }

    protected function getOptions(): array
    {
        return
            [
                ['model', 'm', InputOption::VALUE_REQUIRED, 'The name of the model'],
            ];
    }

    protected function getPath($name): string
    {
        return $this->laravel->databasePath().'/seeders/Modules/'.$name.'.php';
    }

}

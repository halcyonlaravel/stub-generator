<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Resources;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\BaseBasicGenerator;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;
use Illuminate\Support\Str;

class ViewBladeMakeCommand extends BaseBasicGenerator
{
    protected $name = Helper::PREFIX_COMMAND.'make:view-blade:backend';

    protected $description = 'Create a new backend blade files for '.Helper::PROJECT_NAME;

    protected string $type = 'View blades';

    public function stubs(string $name): array
    {
        $path = $this->laravel->resourcePath(
            'views'.DIRECTORY_SEPARATOR.
            'backend'.DIRECTORY_SEPARATOR.
            Str::plural(Str::snake(Str::studly($name), '-')).DIRECTORY_SEPARATOR
        );

        return [
            __DIR__.'/../../../stubs/backend/resource/partials/fields.stub' => $path.'partials'.DIRECTORY_SEPARATOR.'fields.blade.php',
            __DIR__.'/../../../stubs/backend/resource/partials/links.stub' => $path.'partials'.DIRECTORY_SEPARATOR.'links.blade.php',
            __DIR__.'/../../../stubs/backend/resource/partials/overview.stub' => $path.'partials'.DIRECTORY_SEPARATOR.'overview.blade.php',
            __DIR__.'/../../../stubs/backend/resource/create.stub' => $path.'create.blade.php',
            __DIR__.'/../../../stubs/backend/resource/edit.stub' => $path.'edit.blade.php',
            __DIR__.'/../../../stubs/backend/resource/index.stub' => $path.'index.blade.php',
            __DIR__.'/../../../stubs/backend/resource/show.stub' => $path.'show.blade.php',
        ];
    }
}

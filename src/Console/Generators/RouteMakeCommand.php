<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;
use Illuminate\Support\Str;

class RouteMakeCommand extends BaseBasicGenerator
{
    protected $name = Helper::PREFIX_COMMAND.'make:route:backend';

    protected $description = 'Create a new route for '.Helper::PROJECT_NAME;

    protected string $type = 'Route';

    public function stubs(string $name): array
    {
        return
            [
                __DIR__.'/../../stubs/backend/route/resource-table-delete.php.stub'
                => $this->laravel->basePath('routes/web/backend/'.Str::kebab($this->getModelInput()).'.php'),
            ];
    }
}

<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;
use Symfony\Component\Console\Input\InputOption;

class ControllerMakeCommand extends \Illuminate\Routing\Console\ControllerMakeCommand
{
    protected $name = Helper::PREFIX_COMMAND.'make:controller:backend';

    protected $description = 'Create a new backend controller for '.Helper::PROJECT_NAME;

    protected function getStub(): string
    {
        $stub = null;
        $this->type = 'Resource controller';

//        if ($this->option('table')) {
//            $this->type = 'Table controller';
////            $stub = __DIR__.'/../../stubs/backend/controller.table.stub';
//        } else
        if ($this->option('delete')) {
            $this->type = 'Deleted controller';
            $stub = __DIR__.'/../../stubs/backend/controller.delete.stub';
        }

        return $stub ?? __DIR__.'/../../stubs/backend/controller.resource.stub';
    }


    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions(): array
    {
        return array_merge(
            parent::getOptions(),
            [
                ['resource', 'r', InputOption::VALUE_NONE, 'Generate a resource controller class.'],
//                ['table', 't', InputOption::VALUE_NONE, 'Generate a table controller class.'],
                ['delete', 'd', InputOption::VALUE_NONE, 'Generate a delete controller class.'],
            ]
        );
    }

    /**
     * Build the model replacement values.
     *
     * @param  array  $replace
     *
     * @return array
     */
    protected function buildModelReplacements(array $replace): array
    {
        return Helper::buildModelReplacements(
            app()->getNamespace().$this->option('model'),
            $replace
        );
    }

}

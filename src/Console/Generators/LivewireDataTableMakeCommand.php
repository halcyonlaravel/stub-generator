<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Console\Generators;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Helper;

class LivewireDataTableMakeCommand extends BaseBasicGenerator
{
    protected $name = Helper::PREFIX_COMMAND.'make:livewire-datatable';

    protected $description = 'Create a new livewire datatable for '.Helper::PROJECT_NAME;

    protected string $type = 'Livewire Datatable';

    public function stubs(string $name): array
    {
        return
            [
                __DIR__.'/../../stubs/livewire-datatable.stub'
                => $this->laravel->basePath(
                    'app/Http/Livewire/Backend/'.$this->getModelInput().'/'.$this->getModelInput().'Table.php'
                ),
            ];
    }
}
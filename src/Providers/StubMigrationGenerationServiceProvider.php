<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Providers;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Database\MigrateMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Database\MigrationCreator;
use Illuminate\Support\ServiceProvider;

class StubMigrationGenerationServiceProvider extends ServiceProvider
{

    protected $commands = [
        'MigrateMake' => 'halcyon.command.migrate.make',
    ];

    public function register()
    {
        $this->registerCreator();

        $this->registerCommands($this->commands);
    }

    public function provides()
    {
        return array_merge(
            [
                'halcyon.migration.creator',
            ],
            array_values($this->commands)
        );
    }

    protected function registerCreator()
    {
        $this->app->singleton(
            'halcyon.migration.creator',
            fn($app) => new MigrationCreator($app['files'], __DIR__.'/../stubs/database')
        );
    }

    protected function registerCommands(array $commands)
    {
        foreach (array_keys($commands) as $command) {
            call_user_func_array([$this, "register{$command}Command"], []);
        }

        $this->commands(array_values($commands));
    }

    protected function registerMigrateMakeCommand()
    {
        $this->app->singleton(
            'halcyon.command.migrate.make',
            function ($app) {
                // Once we have the migration creator registered, we will create the command
                // and inject the creator. The creator is responsible for the actual file
                // creation of the migrations, and may be extended by these developers.
                $creator = $app['halcyon.migration.creator'];

                $composer = $app['composer'];

                return new MigrateMakeCommand($creator, $composer);
            }
        );
    }
}

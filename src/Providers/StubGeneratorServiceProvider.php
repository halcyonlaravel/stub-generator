<?php

namespace HalcyonLaravelBoilerplate\StubGenerator\Providers;

use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\BreadcrumbsMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\ControllerMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Database\FactoryMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Database\SeederMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\LivewireDataTableMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\ModelMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\PolicyMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\RepositoryMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\RepositoryObserverMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\Resources\ViewBladeMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\RouteMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\Generators\TestMakeCommand;
use HalcyonLaravelBoilerplate\StubGenerator\Console\ResourceGenerateCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class StubGeneratorServiceProvider extends PackageServiceProvider
{
    protected function getPackageBaseDir(): string
    {
        return __DIR__.DIRECTORY_SEPARATOR.'..';
    }

    public function configurePackage(Package $package): void
    {
        $package
            ->name('stub-generator')
            ->hasCommands(
                [
                    ResourceGenerateCommand::class,
                    ModelMakeCommand::class,
                    ControllerMakeCommand::class,
                    RepositoryMakeCommand::class,
                    SeederMakeCommand::class,
                    FactoryMakeCommand::class,
                    ViewBladeMakeCommand::class,
                    RouteMakeCommand::class,
                    BreadcrumbsMakeCommand::class,
                    TestMakeCommand::class,
                    PolicyMakeCommand::class,
                    RepositoryObserverMakeCommand::class,
                    LivewireDataTableMakeCommand::class,
                ]
            );
    }
}

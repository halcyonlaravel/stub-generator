# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## Unreleased - TBD

### Added

- Add support for livewire datatable v2.

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## 4.0.1 - 2021-08-11

### Added

- Nothing.

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Remove DummyTableController in resource route stub.

- Remove deleted.blade.php.

- Remove duplicate changelog detail.

### Fixed

- Nothing.

## 4.0.0 - 2021-07-19

### Added

- Add liveweire datatable generator command.

- Add rappasoft/laravel-livewire-tables:^1.11 in composer require.

- Add todo for spatie/laravel-signal-aware-command, require php8.

- Add halcyon money in repositories in composer.json.

### Changed

- Implement spatie/laravel-package-tools ^1.9.

- Optimise code.

- Update stub for latest boilerplate.

- Set core-base version minimum to v7.2.3.

### Deprecated

- Nothing.

### Removed

- Drop support core-base 6 and below.

### Fixed

- Nothing.
